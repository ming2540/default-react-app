import Cookies from 'js-cookie';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { PATH } from '../../routes/CustomRouter';


export const MENU = {
  FIRST: 'first',
  SECOND: 'second'
}

function MenuBar(props) {
  const {
    active,
    onSelectMenu,
  } = props;

  const navigate = useNavigate();

  const handleToggleLogOut = () => {
    Cookies.remove('token');
    navigate(PATH.LOGIN);
  }

  const renderMenuElement = () => {
    return Object.values(MENU).map((menu, idx) =>
      <button onClick={() => onSelectMenu(menu)}>{menu}</button>
    );
  }

  return (
    <div>
      menu:
      {active}
      {renderMenuElement()}
      <button onClick={handleToggleLogOut}>logout</button>
    </div>
  )
}

export default MenuBar;