import Cookies from 'js-cookie';
import React from 'react';
import { BrowserRouter, Routes, Route, Outlet, Navigate } from 'react-router-dom';
import LoginScreen from '../screens/LoginScreen';
import MainScreen from '../screens/MainScreen';


export const PATH = {
  LOGIN: '/login',
  MAIN: '/',
}


function PrivateOutlet() {
  const auth = Cookies.get('token') != null;
  return auth ? <Outlet /> : <Navigate to={PATH.LOGIN} />;
}


function CustomRouter(){
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path={PATH.LOGIN} element={<LoginScreen/>} />
          <Route path={PATH.MAIN} element={<PrivateOutlet/>} >
            <Route path='' element={<MainScreen/>} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default CustomRouter;