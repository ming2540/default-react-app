import React from 'react';
import MenuBar, { MENU } from '../components/common/MenuBar';
import FirstView from '../views/FirstView';
import SecondView from '../views/SecondView';


function MainScreen() {
  const [activeMenu, setActiveMenu] = React.useState(MENU.FIRST)

  const renderView = () => {
    switch (activeMenu) {
      case MENU.FIRST:
        return <FirstView/>
      case MENU.SECOND:
        return <SecondView/>
      default:
        break;
    }
  }

  return (
    <div>
      <MenuBar active={activeMenu} onSelectMenu={(menu) => setActiveMenu(menu)}/>
      Main screen
      {renderView()}
    </div>
  )
}

export default MainScreen;