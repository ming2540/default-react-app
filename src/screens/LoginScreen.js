import Cookies from 'js-cookie';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { PATH } from '../routes/CustomRouter';


function LoginScreen() {
  const navigate = useNavigate();

  const handleToggleLogIn = () => {
    Cookies.set('token', 'mock token');
    navigate(PATH.MAIN);
  }

  return (
    <div>
      Login screen
      <button onClick={handleToggleLogIn}>log in</button>
    </div>
  )
}

export default LoginScreen;